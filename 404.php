<?php get_header(); ?>
<section class="section">
    <div class="container">
        <div class="error-404 not-found">
            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentynineteen' ); ?></h1>
            </header>
            <div class="page-content">
                <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentynineteen' ); ?></p>
                <?php get_search_form(); ?>
                <a href="<?php echo home_url( '/' );?>" class="btn btn-secondary btn-lg">Вернуться на главную</a>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>

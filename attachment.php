<?php get_header(); ?>
    <section class="wp-article">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <?php
                        if(have_posts()){
                            while (have_posts()){
                                the_post();
                                global $post;
                                ?>
                                <h1><?php the_title(); ?></h1>
                                <div class="wp-article-content">
                                    <a href="<?php echo $post->guid; ?>">Скачать</a>
                                    <?php 
                                        the_content(); 
                                    
                                    ?>
                                </div>
                                <?php 
                                    if( comments_open() || get_comments_number() ){
                                        comments_template(); 
                                    }
                                ?>
                                
                            <?php }
                        }
                    ?>
                </div>
                <div class="col-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
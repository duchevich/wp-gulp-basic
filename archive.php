<?php get_header(); ?>
    <section class="section">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h1><?php the_archive_title() ?></h1>
                <p><?php the_archive_description() ?></p>
                <?php
                    if(have_posts()){
                        while (have_posts()){
                            the_post();
                            get_template_part( 'inc/templates/post/content' );
                        }
                    }
                ?>
            </div>
            <div class="col-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
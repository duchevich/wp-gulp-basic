<?php get_header(); ?>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <?php
                    if( have_posts() ){ 
                        while( have_posts() ){ 
                            the_post();
                            get_template_part( 'inc/templates/post/content' );
                        }
                        previous_posts_link();
                        the_posts_pagination();
                        next_posts_link(); 
                    }
                ?>
            </div>
            <div class="col-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
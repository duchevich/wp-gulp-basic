<?php
    function dwd_customize_register( $wp_customize){
        // echo '<pre>';
        // var_dump( $wp_customize );
        // echo '</pre>';

        $wp_customize->get_section( 'title_tagline' )->title    =   'General';

        $wp_customize->add_panel( 'mytheme', [
            'title'         =>  __( 'Основные данные сайта', 'mytheme' ),
            'description'   =>  '<p>Контактные данные и глобальные настройки</p>',
            'priority'      =>  60
        ]);

        dwd_social_customizer_section( $wp_customize );
        dwd_misc_customizer_section( $wp_customize );
        dwd_social_customizer_section( $wp_customize );
    }
<?php
    function dwd_setup_theme() {
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'html5' );


        register_nav_menus( [
            'header_menu' => __('Меню в хедере', TH_DOMAIN),
            'footer_menu' => __('Меню в футере', TH_DOMAIN)
        ] );
    }
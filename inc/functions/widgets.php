<?php
    function dwd_widgets(){
        register_sidebar( array(
            'name' => __( 'Main Sidebar', TH_DOMAIN ),
            'id' => 'dwd_sidebar',
            'description' => __( 'Widgets in this area will be shown on all posts and pages.', TH_DOMAIN ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widgettitle">',
            'after_title'   => '</h2>',
        ) );
    }
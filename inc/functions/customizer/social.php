<?php
    
    function dwd_social_customizer_section( $wp_customize ){
        $wp_customize->add_setting( 'dwd_facebook_handle', [ 
            'default'   =>  ''
        ]);

        $wp_customize->add_setting( 'dwd_instagram_handle', array(
            'default'                   =>  '',
        ));

        $wp_customize->add_setting( 'dwd_email', array(
            'default'                   =>  '',
        ));

        $wp_customize->add_setting( 'dwd_phone_number', array(
            'default'                   =>  '',
        ));

        $wp_customize->add_section( 'dwd_social_section', [
            'title'     =>  __( 'Контактные данные', TH_DOMAIN ),
            'priority'  =>  30, 
            'panel'     =>  'mytheme'
        ]);

        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            'dwd_social_facebook_input',
            array(
                'label'          => __( 'Facebook', TH_DOMAIN ),
                'section'        => 'dwd_social_section',
                'settings'       => 'dwd_facebook_handle'
            )
        ));

        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            'dwd_social_instagram_input',
            array(
                'label'                 =>  __( 'Instagram', TH_DOMAIN ),
                'section'               => 'dwd_social_section',
                'settings'              => 'dwd_instagram_handle',
                'type'                  =>  'text'
            )
        ));

        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            'dwd_social_email_input',
            array(
                'label'                 =>  __( 'Email', TH_DOMAIN ),
                'section'               => 'dwd_social_section',
                'settings'              => 'dwd_email',
                'type'                  =>  'text'
            )
        ));

        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            'dwd_social_phone_number_input',
            array(
                'label'                 =>  __( 'Phone Number', TH_DOMAIN ),
                'section'               => 'dwd_social_section',
                'settings'              => 'dwd_phone_number',
                'type'                  =>  'text'
            )
        ));
    }
<?php
    function dwd_enqueue(){
        $uri = get_theme_file_uri();
        $ver = 1.0;
        wp_register_style('style', $uri . '/assets/css/main.min.css', [], $ver);
        wp_enqueue_style('style');
        wp_register_script('js', $uri . '/assets/js/main.min.js', [], $ver, true);
        wp_enqueue_script('js');
    }
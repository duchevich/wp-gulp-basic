<div class="post">
    <div class="post-title">
        <h2>
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h2>
    </div>
    <?php if( has_post_thumbnail() ){ ?>
        <div class="post-thumb">
            <a href="<?php the_permalink(); ?>"> 
                <?php the_post_thumbnail( 'full', [ 'class' => 'post-img' ]); ?>
            </a>
        </div>
    <?php } ?>
    <div class="post-content">
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="post-more">Read More</a>
    </div>
    <ul class="post-meta wrapper">
        <li><?php echo get_the_date(); ?></li>
        <li>
            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                <?php the_author(); ?>
            </a>
        </li>
        <li>
            <?php the_category( ' ' ); ?>
        </li>
        <li>
            <a href="#">
                <?php comments_number( '0' ); ?>
            </a>
        </li>
    </ul>
</div>
<?php
    /**
     * Setup
     */
    define('TH_DOMAIN', 'mytheme');


    /**
     * Includes
     */
    include(get_theme_file_path('/inc/functions/front/enqueue.php'));
    include(get_theme_file_path('/inc/functions/setup.php'));
    // include(get_theme_file_path('/inc/functions/custom-nav-walker.php'));
    include(get_theme_file_path('/inc/functions/widgets.php'));
    // include(get_theme_file_path('/inc/functions/theme-customizer.php'));
    // include(get_theme_file_path('/inc/functions/customizer/social.php'));
    // include(get_theme_file_path('/inc/functions/customizer/misc.php'));
    // include(get_theme_file_path('/inc/functions/customizer/enqueue.php'));

    /**
     * Hooks
     */
    add_action('wp_enqueue_scripts', 'dwd_enqueue');
    add_action('after_setup_theme', 'dwd_setup_theme');
    add_action('widgets_init', 'dwd_widgets');
    // add_action('customize_register', 'dwd_customize_register');

    /**
     * Shortcodes
     */

    


    // add_action( 'init', 'create_post_type' );
	// function create_post_type() {	
	// 	register_post_type( 'our_services',array('labels' => array('name' => __( 'Наш сервис' ),'singular_name' => __( 'Unit' ),'add_new' => 'Add new unit' ,'add_new_item' => 'New unit',), 'rewrite' => true,'public' => true,'has_archive' => true,'supports' => array('title', 'thumbnail'),));
	// }
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="header">
        <div class="container wrapper">
            <div class="left wrapper">
                <a href="/" class="logolink">
                    Logo
                </a>
                <?php
                    if(has_nav_menu('header_menu')){
                        wp_nav_menu([
                            'theme_location'  => 'header_menu',
                            'container'       => false, 
                            'menu_class'      => 'menu-list wrapper', 
                            'fallback_cb'     => false,
                            // 'walker'          => new DWD_Custom_Nav_Walker
                        ]);
                    }
                ?>
            </div>
            <div>
                <?php
                    if(get_theme_mod('dwd_phone_number')){
                        echo ' <a href="tel:+">' . get_theme_mod('dwd_phone_number') . '</a>';
                    }
                ?>
            </div>
            <button type="button" class="button menu-button">
                <span class="sr-only">Toggle navigation</span>
                <span class="menu-icon"></span>
                <span class="menu-icon"></span>
                <span class="menu-icon"></span>
            </button>
        </div>
    </header>

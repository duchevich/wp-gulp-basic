<?php get_header(); ?>
    <section class="wp-article">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div id="post-<?php the_ID()?> <?php post_class('post')?>>
                        <?php
                            if(have_posts()){
                                while (have_posts()){
                                    the_post();
                                    global $post;
                                    $author_ID = $post->post_author;
                                    $author_url = get_author_posts_url( $author_ID );
                                    ?>
                                    <h1><?php the_title(); ?></h1>
                                    <?php if( has_post_thumbnail() ){ ?>
                                        <div class="wp-article-thumb">
                                            <a href="<?php the_permalink(); ?>"> 
                                                <?php the_post_thumbnail( 'full' ); ?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <div class="wp-article-content">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php wp_link_pages() ?>
                                    <div class="wp-article-tags">
                                        <?php the_tags(); ?>
                                    </div>

                                    <div class="wp-article-author wrapper">
                                        <div class="wp-article-author-image">
                                            <?php echo get_avatar( $author_ID, 90, '', false, [ 'class' => 'img-circle' ] ); ?>
                                        </div>
                                        <div class="wp-article-author-desc">
                                            <a href="<?php echo $author_url; ?>">
                                                <?php the_author(); ?>
                                            </a>
                                            <?php echo nl2br( get_the_author_meta( 'description' ) ); ?>
                                        </div>
                                    </div>

                                    <ul class="wp-article-meta wrapper">
                                        <li><?php echo get_the_date(); ?></li>
                                        <li>
                                            <?php the_category( ' ' ); ?>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <?php comments_number( '0' ); ?>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="wp-article-nav wrapper">
                                        <?php 
                                            previous_post_link();
                                            next_post_link(); 
                                        ?>
                                    </div>
                                    <?php 
                                        if( comments_open() || get_comments_number() ){
                                            comments_template(); 
                                        }
                                    ?>
                                    
                                <?php }
                            }
                        ?>
                        <h4>Related Posts:</h4>
                        <div class="related-posts clearfix">
                            <?php 
                            $categories             =   get_the_category();
                            $rp_query               =   new WP_Query([
                                'posts_per_page'    =>  2,
                                'post__not_in'      =>  [ $post->ID ],
                                'cat'               =>  !empty($categories) ?  $categories[0]->term_id : null
                            ]);
                            if( $rp_query->have_posts() ){
                                while( $rp_query->have_posts() ){
                                    $rp_query->the_post();
                                    ?>
                                    <div class="mpost clearfix">
                                        <?php
                                        if( has_post_thumbnail() ){
                                            ?>
                                            <div class="entry-image">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail( 'thumbnail' ); ?>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <ul class="entry-meta clearfix">
                                                <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
                                                <li><i class="icon-comments"></i> <?php comments_number( '0' ); ?></li>
                                            </ul>
                                            <div class="entry-content">
                                                <?php the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                        </div>
                    </div>
                <div class="col-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
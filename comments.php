<?php
    if( post_password_required() ){
        return;
    }
?>

<div class="">
    <?php 
        if( have_comments() ){
        ?>
        <h3><?php comments_number(); ?></h3>
        <div class="">
            <?php
            foreach( $comments as $comment ){
                ?>
                <div class=" wrapper">
                    <div class="comment-meta">
                        <div class="comment-author">
                            <span class="comment-avatar">
                                <?php echo get_avatar( $comment, 60, '', '', [ 'class' => '' ] ); ?>
                            </span>
                        </div>
                    </div>
                    <div class="comment-content">
                        <div class="comment-author">
                            <?php comment_author(); ?><br>
                            <span><?php comment_date(); ?></span>
                        </div>
                        <?php comment_text(); ?>
                    </div>
                </div>
                <?php
            }
            the_comments_pagination(); 
            ?>
        </div>
        <?php
    }
    ?>

    <div class="">
        <?php
        comment_form([
            'comment_field'         =>  '<div class="col_full">
                                            <label>Comment</label>
                                            <textarea name="comment" cols="58" rows="7" class="sm-form-control"></textarea>
                                        </div>',
            'fields'                =>  [
                                            'author'            =>  
                                                '<div class="col_one_third">
                                                    <label>' . __( 'Name', 'udemy' ) . '</label>
                                                    <input type="text" name="author" class="sm-form-control" />
                                                </div>',
                                            'email'             =>  
                                                '<div class="col_one_third">
                                                    <label>' . __( 'Email', 'udemy' ) . '</label>
                                                    <input type="text" name="email" class="sm-form-control" />
                                                </div>',
                                            'url'               =>  
                                                '<div class="col_one_third col_last">
                                                    <label>' . __( 'Website', 'udemy' ) . '</label>
                                                    <input type="text" name="url" class="sm-form-control" />
                                                </div>'
                                        ],
            'class_submit'          =>  'button button-3d nomargin',
            'label_submit'          =>  __( 'Submit Comment', 'udemy' ),
            'title_reply'           =>  __( 'Leave a <span>Comment</span>', 'udemy' )
        ]);
        ?>
    </div>
</div>
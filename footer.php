<footer class="footer">
    <div class="container wrapper">
        <a href="" class="footer-logo">
            <img class="footer-bigidImg" src="" alt="BIGID логотип">
            <p class="footer-deviz"></p>
        </a>
        <a href="" class="footer-politic" data-toggle="modal" data-target="#politicModal">Политика конфиденциальности</a>
        <a href="" class="footer-privacy" data-toggle="modal" data-target="#privacyModal">Пользовательское соглашение</a>
        <a href="" class="footer-bigid">
            <img class="footer-bigidImg" src="" alt="BIGID логотип">
        </a>
    </div>
</footer>
<?php get_template_part( 'inc/templates/modals'); ?>
<?php wp_footer(); ?>
</body>
</html>
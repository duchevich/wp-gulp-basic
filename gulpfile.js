'use strict';
var hostName  = 'http://wordpress.loc/';
var buildFolder  = 'D:/OSPanel/domains/wordpress.loc/wp-content/themes/mytheme/';
/* параметры для gulp-autoprefixer */
var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

/* пути к исходным файлам (src), к готовым файлам (build), а также к тем, за изменениями которых нужно наблюдать (watch) */
var path = {
    build: {
        html: buildFolder,
        inc: buildFolder + '/inc/',
        lang: buildFolder + '/lang/',
        js: buildFolder + '/assets/js/',
        css: buildFolder + '/assets/css/',
        img: buildFolder + '/assets/img/',
        video: buildFolder + '/assets/video/',
        fonts: buildFolder + '/assets/fonts/',
        screen: buildFolder,
        theme:  buildFolder
    },
    src: {
        html: '*.php',
        inc: 'inc/**/*.*',
        lang: 'lang/*.*',
        js: 'assets/js/main.js',
        style: 'assets/style/main.scss',
        img: 'assets/img/**/*.*',
        video: 'assets/video/**/*.*',
        fonts: 'assets/fonts/**/*.*',
        screen: 'screenshot.png',
        theme: 'style.css'
    },
    watch: {
        html: '*.php',
        inc: 'inc/**/*.*',
        lang: 'lang/*.*',
        js: 'assets/js/**/*.js',
        css: 'assets/style/**/*.*',
        img: 'assets/img/**/*.*',
        video: 'assets/video/**/*.*',
        fonts: 'assets/fonts/**/*.*',
        theme: 'style.css'
    },
    clean:  buildFolder
};

/* настройки сервера */
var config = {
    proxy: hostName,
        port: 3010,
    notify: false
};

/* подключаем gulp и плагины */
var gulp = require('gulp'),  // подключаем Gulp
    webserver = require('browser-sync'), // сервер для работы и автоматического обновления страниц
    plumber = require('gulp-plumber'), // модуль для отслеживания ошибок
    rigger = require('gulp-rigger'), // модуль для импорта содержимого одного файла в другой
    sourcemaps = require('gulp-sourcemaps'), // модуль для генерации карты исходных файлов
    sass = require('gulp-sass'), // модуль для компиляции SASS (SCSS) в CSS
    autoprefixer = require('gulp-autoprefixer'), // модуль для автоматической установки автопрефиксов
    cleanCSS = require('gulp-clean-css'), // плагин для минимизации CSS
    uglify = require('gulp-uglify'), // модуль для минимизации JavaScript
    cache = require('gulp-cache'), // модуль для кэширования
    imagemin = require('gulp-imagemin'), // плагин для сжатия PNG, JPEG, GIF и SVG изображений
    jpegrecompress = require('imagemin-jpeg-recompress'), // плагин для сжатия jpeg	
    pngquant = require('imagemin-pngquant'), // плагин для сжатия png
    rimraf = require('gulp-rimraf'), // плагин для удаления файлов и каталогов
    rename = require('gulp-rename');

/* задачи */

// запуск сервера
gulp.task('webserver', function () {
    webserver(config);
});

// сбор html
gulp.task('html:build', function () {
    return gulp.src(path.src.html) // выбор всех html файлов по указанному пути
        .pipe(plumber()) // отслеживание ошибок
        .pipe(rigger()) // импорт вложений
        .pipe(gulp.dest(path.build.html)) // выкладывание готовых файлов
        .pipe(webserver.reload({ stream: true })); // перезагрузка сервера
});

// сбор стилей
gulp.task('css:build', function () {
    return gulp.src(path.src.style) // получим main.scss
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init()) // инициализируем sourcemap
        .pipe(sass()) // scss -> css
        .pipe(autoprefixer({ // добавим префиксы
            overrideBrowserslist: autoprefixerList
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(rename({ suffix: '.min' }))
        .pipe(cleanCSS()) // минимизируем CSS
        .pipe(sourcemaps.write('./')) // записываем sourcemap
        .pipe(gulp.dest(path.build.css)) // выгружаем в build
        .pipe(webserver.reload({ stream: true })); // перезагрузим сервер
});

// сбор js
gulp.task('js:build', function () {
    return gulp.src(path.src.js) // получим файл main.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(rigger()) // импортируем все указанные файлы в main.js
        .pipe(gulp.dest(path.build.js))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.build.js)) // положим готовый файл
        .pipe(webserver.reload({ stream: true })); // перезагрузим сервер
});

// перенос шрифтов
gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});
gulp.task('screen:build', function () {
    return gulp.src(path.src.screen)
        .pipe(gulp.dest(path.build.screen));
});
gulp.task('theme:build', function () {
    return gulp.src(path.src.theme)
        .pipe(gulp.dest(path.build.theme));
});
gulp.task('video:build', function () {
    return gulp.src(path.src.video)
        .pipe(gulp.dest(path.build.video));
});
gulp.task('inc:build', function () {
    return gulp.src(path.src.inc)
        .pipe(gulp.dest(path.build.inc))
        .pipe(webserver.reload({ stream: true }));
});
gulp.task('lang:build', function () {
    return gulp.src(path.src.lang)
        .pipe(gulp.dest(path.build.lang));
});
// обработка картинок
gulp.task('image:build', function () {
    return gulp.src(path.src.img) // путь с исходниками картинок
        .pipe(cache(imagemin([ // сжатие изображений
            imagemin.gifsicle({ interlaced: true }),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            imagemin.svgo({ plugins: [{ removeViewBox: false }] })
        ])))
        .pipe(gulp.dest(path.build.img))
        .pipe(webserver.reload({ stream: true }));
});

// удаление каталога build 
gulp.task('clean:build', function () {
    return gulp.src(path.clean, { read: false })
        .pipe(rimraf());
});

// очистка кэша
gulp.task('cache:clear', function () {
    cache.clearAll();
});

// сборка
gulp.task('build',
    gulp.series('clean:build',
        gulp.parallel(
            'html:build',
            'inc:build',
            'lang:build',
            'css:build',
            'js:build',
            'fonts:build',
            'screen:build',
            'theme:build',
            'video:build',
            'image:build'
        )
    )
);

// запуск задач при изменении файлов
gulp.task('watch', function () {
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.inc, gulp.series('inc:build'));
    gulp.watch(path.watch.lang, gulp.series('lang:build'));
    gulp.watch(path.watch.css, gulp.series('css:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
    gulp.watch(path.watch.theme, gulp.series('theme:build'));
    gulp.watch(path.watch.video, gulp.series('video:build'));
});

// задача по умолчанию
gulp.task('default', gulp.series(
    'build',
    gulp.parallel('webserver','watch')      
));

<?php get_header(); ?>
    <section class="page">
    <div class="container">
        <div class="row">
            <div class="col-8">
            <h1><?php the_title(); ?></h1>
                <?php
                    while (have_posts()){
                        the_post();
                        ?>
                        <?php if( has_post_thumbnail() ){ ?>
                            <div class="post-thumb">
                                <a href="<?php the_permalink(); ?>"> 
                                    <?php the_post_thumbnail( 'full' ); ?>
                                </a>
                            </div>
                        <?php } ?>
                        <?php the_content(); ?>
                        <?php wp_link_pages() ?>
                        <div class="post-tags">
                            <?php the_tags(); ?>
                        </div>
                        <?php 
                            if( comments_open() || get_comments_number() ){
                                comments_template(); 
                            }
                        ?>
                        
                    <?php }
                ?>
            </div>
            <div class="col-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>